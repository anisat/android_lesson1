package com.anisat.android.helloapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * @author Anisat
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
